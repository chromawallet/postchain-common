var asn = require('asn1.js');

var ASNBuffer = asn.define('Buffer', function() {
    this.octstr();
});

var ASNArgType = asn.define('ArgType', function () {
    this.enum({
        0: 'string',
        1: 'bytearray',
        2: 'number'});
});

var ASNTypedArg = asn.define('TypedArg', function() {
    this.seq().obj(
        this.key('type').use(ASNArgType),
        this.key('value').octstr()
    );
});

var ASNArg = asn.define('Arg', function() {
    this.choice({
            array: this.seqof(ASNArg),
            primitive: this.use(ASNTypedArg)}
    );
});

var ASNFunctionCall = asn.define('FunctionCall', function() {
    this.seq().obj(
        this.key('functionName').utf8str(),
        this.key('args').seqof(ASNArg));
});

var ASNGenericMessage = asn.define('GenericMessage', function () {
    this.seq().obj(
        this.key('calls').seqof(ASNFunctionCall),
        this.key('signers').seqof(ASNBuffer),
        this.key('signatures').optional().seqof(ASNBuffer))
});

/**
 * @param bytes the raw client message
 * @returns {calls:      [{functionName: <funcName>, args: [arg1, arg2, ...]}, ...],
 *           signatures: [{pubKey: <buffer>, signature: <buffer>}]}
 */
module.exports.decode = (bytes) => {
    var rawObject = ASNGenericMessage.decode(bytes);
    rawObject.calls.forEach(call => {
        call.args = call.args.map(arg => parseValue(arg));
    });
    return rawObject;
};

module.exports.encode = (callsAndSignatures) => {
    var preparedCalls = callsAndSignatures.calls.map((call) => {
        return {functionName: call.functionName,
                args: call.args.map((arg) => createTypedArg(arg))}
    });
    return ASNGenericMessage.encode({calls: preparedCalls, signers: callsAndSignatures.signers, signatures: callsAndSignatures.signatures});
};

module.exports.encodeArg = (arg) => {
    return ASNArg.encode(createTypedArg(arg))
};

module.exports.decodeArg = (bytes) => {
    const obj = ASNArg.decode(bytes);
    return parseValue(obj);
};


function parseValue(typedArg) {
    // console.log("Typedarg:", typedArg);
    if (typedArg.type === 'primitive') {
        var type = typedArg.value.type;
        var buffer = typedArg.value.value;
        if (type === 'number') {
            var value = buffer.readIntBE(0, 6);
            console.log(`${buffer.toString('hex')} is decoded as ${value}`);
            return value;
        }
        if (type === 'string') {
            return buffer.toString('utf8');
        }
        if (type === 'bytearray') {
            return buffer;
        }
    }
    if (typedArg.type === 'array') {
        return typedArg.value.map(item => parseValue(item));
    }
    throw new Error(`Cannot parse typedArg ${typedArg}. Unknown type ${typedArg.type}`);
}

function createTypedArg(value) {
    try {
        if (value.constructor === Array) {
            return {type: 'array', value: value.map(item => createTypedArg(item))};
        }
        if (Buffer.isBuffer(value)) {
            return {type: 'primitive', value: {type: 'bytearray', value: value}};
        }
        if (typeof value === 'number') {
            var buffer = Buffer.alloc(8);
            buffer.writeIntBE(value, 0, 6);
            // console.log(`Integer ${value} is encoded as ${buffer.toString('hex')}`);
            return {type: 'primitive', value: {type: 'number', value: buffer}};
        }
        if (typeof value === 'string') {
            var buffer = Buffer.from(value, 'utf8');
            return {type: 'primitive', value: {type: 'string', value: value}};
        }
    } catch (error) {
        throw new Error(`Failed to encode ${value.toString()}: ${error}`);
    }
    throw new Error(`value ${value} have unsupported type: ${typeof value}`);
}